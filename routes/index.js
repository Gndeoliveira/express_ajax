var express = require('express');
var router = express.Router();
var connMysql = require('../config/dbConn.js')

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index');
});

router.get('/listar_usuario', function (req, res, next) {
  connMysql.query('select * from usuarios', function (err, result) {
    console.log('dados usuarios: ', result);
    setTimeout(function () {
      // res.render('cadastrar_usuario'); 
      res.render('listar_usuario', { data: result });
     }, 1000)
  })
});

router.get('/cadastrar_usuario', function (req, res, next) {
  setTimeout(function () {
     res.render('cadastrar_usuario'); 
    }, 1000)
});

router.get('/editar_usuario', function (req, res, next) {
  console.log('parametro: ', Object.keys(req.query).toString());
  var query = Object.keys(req.query).toString()
  connMysql.query('select * from usuarios where id =' + query, function (err, result) {
    console.log('resultado: ', result);
    setTimeout(function () {
      res.render('editar_usuario', { data: result[0] });
     }, 1000)
  })
});

router.get('/sobre', function (req, res, next) {
  res.render('sobre', { title: 'Express' });
});
//salvar
router.post('/salvar_usuario', function (req, res, next) {
  var { nome, telefone, email, sexo } = req.body
  var error = []

  if (nome === '') {
    console.log('nome não')
    error.push('Campo nome não pode ser vazio')
  }
  if (telefone === '') {
    console.log('nome não')
    error.push('Campo telefone não pode ser vazio')
  }
  if (email === '') {
    console.log('nome não')
    error.push('Campo email não pode ser vazio')
  }
  if (sexo === '') {
    console.log('nome não')
    error.push('Campo sexo não pode ser vazio')
  }

  console.log('dados do erro ', error)


  if (nome !== '' && telefone !== '' && email !== '' && sexo !== '') {
    let data = {
      nome,
      telefone,
      email,
      sexo
    }
    connMysql.query('insert into usuarios set ?', data)
    setTimeout(function () {
      res.send({ result: data });
      // res.render('cadastrar_usuario'); 
     }, 1000)
  } else {
    setTimeout(function () {
      // res.render('cadastrar_usuario'); 
      res.send({ result: error })
     }, 1000)
  }

});
// Deletar
router.delete('/deletar_usuario', function (req, res, next) {
  var data = req.url
  console.log('dados recebidos ', data);
  connMysql.query('delete from usuarios where id = ' +
    data.replace("/deletar_usuario?", ""))

    setTimeout(function () {
      // res.render('cadastrar_usuario'); 
      res.send({ result: data });
     }, 1000)
});
// Atualizar
router.put('/editar_usuario', function (req, res, next) {
  
  console.log('corpo ', req.body.id);
  var { nome, telefone, email, sexo } = req.body
  var data = {
    nome,
    telefone,
    email,
    sexo
  }
  connMysql.query('update usuarios set ' + (`nome = '${data.nome}', telefone = '${data.telefone}', email = '${data.email}', sexo = '${data.sexo}'`) + ' where id = ' + req.body.id)
  // connMysql.query('update usuarios set ' + data + ' where id =' + req.body.id)

  setTimeout(function () {
    res.send({ result: data });
    // res.render('cadastrar_usuario'); 
   }, 1000)
});

module.exports = router;
